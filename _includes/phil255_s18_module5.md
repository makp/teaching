
# Week 14

-   05-01

    -   J. Diamond "[Must We Shoot Deer to Save Nature?](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Natural History*

-   05-03

    -   J. Coyne "[The Geography of Life](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" from *Why Evolution is True*


# Week 15

-   05-08

    -   R. Draper "[Madagascar's Pierced Heart](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *National Geographic*

-   05-10

    -   E. Willott "[Restoring Nature, Without Mosquitoes?](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Restoration Ecology*.


# Week 16

-   05-15

    -   Review for the final


# Final Exam

-   May 17th (Thursday) at 3:00 PM.

