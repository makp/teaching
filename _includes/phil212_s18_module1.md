
# Week 1: What Is Complexity?

-   01-30

    -   No required readings.

-   02-01

    -   M. Mitchell ["What is Complexity?"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *Complexity: A Guided Tour*


# Week 2: Darwin's Strange Inversion of Reasoning

-   02-06

    -   R. Dawkins ["Explaining the very improbable"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *The Blind Watchmaker*

-   02-08

    -   D. Dennett ["Algorithms"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *Intuition Pumps and Other Tools for
        Thinking*
    -   E. Young ["Inside the Eye: Nature's Most Exquisite Creation"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)
        *National Geographic*


# Week 3: Doing Adaptive Things

-   02-13

    -   M. Mitchell ["Evolution"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *Complexity: A Guided Tour*.

-   02-15

    1.  M. McFall-Ngai ["Hawaiian Bobtail Squid"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) *Current Biology*.
    2.  C. Zimmer ["Evolution of Feathers"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) *The National Geographic
        Magazine*.


# Week 4: Replicators and Extended Phenotypes

-   02-20

    1.  R. Dawkins ["The Replicators"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *The Selfish Gene*.
    2.  J. Roach ["Parasite Makes Ants Resemble Berries"](https://news.nationalgeographic.com/news/2008/01/080117-ant-berries.html) *National
        Geographic News*.

-   02-22

    1.  J. Diamond ["The Great Leap Forward"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *The Third Chimpanzee*.
    2.  D. Dennett ["Memes"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx) from *Intuition Pumps and Other Tools for
        Thinking*.

