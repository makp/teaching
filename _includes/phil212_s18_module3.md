
# Week 9: Reproduction vs. Growth

-   03-27

    -   D. Skillings "[Life is not easily bounded](https://aeon.co/essays/what-constitutes-an-individual-organism-in-biology)" *Aeon*

-   03-29

    -   R. Dawkins "[Rediscovering the Organism](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *The Extended
        Phenotype*.
    -   M. Grant "[The Trembling Giant](http://discovermagazine.com/1993/oct/thetremblinggian285)" *Discover*


# Week 10: Collective Reproduction

-   04-03

    -   P. Godfrey-Smith "[Darwinian Individuals](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Groups to
        Individuals*.

-   04-05

    -   T. Seeley "[The Honey Bee Colony as a Superorganism](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" *American
        Scientist*.
    -   E. Singer "[Life's Secrets Sought in a Snowflake](https://www.quantamagazine.org/lifes-secrets-sought-in-a-snowflake-20151103/)" *Quanta Magazine*.

