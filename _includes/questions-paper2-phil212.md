## Questions -- Second Paper


### Module 2: Evolution as Tinkering


1.  According to F. Jacob, evolution by natural selection is better
    viewed as a tinkerer rather than an engineer. What are his
    arguments? In addition to Jacob's paper, use examples from Coyne's
    reading to articulate your answer.
2.  In his book *Wonderful Life*, S. J. Gould claims that "our origin
    is the product of a massive historical contingency, and we would
    probably never arise again even if life's tape could be replayed a
    thousand times." (i) Why is this Gould's position? Use Gould's
    interpretation of the Burgess Shale to articulate your answer.
    (ii) Connect Gould's interpretation of the Burgess Shale to
    explain why Gould thinks that Dawkins and Dennett adopt a narrow
    view of evolution.
3. Of course evolution is not progressive in the sense that evolution
   produces a ladder of lower to higher tier organisms, with humans
   being more 'evolved' than other living beings. However, R. Dawkins
   maintains that evolution is progressive if a less anthropocentric
   notion of progress is adopted. Describe Dawkins' concept of
   evolutionary progress, and his arguments for thinking that
   evolution can be progressive.

### Module 3: What are Organisms?

1.  Why is the intuitive concept of reproduction problematic? Use some
    of the examples discussed in class to articulate your answer.
2.  Why is the debate over the concept of reproduction relevant for the
    theory of evolution by natural selection?
3.  Describe and contrast the accounts of reproduction proposed by the
    following authors: (1) D. Janzen; (2) R. Dawkins; and (3) P.
    Godfrey-Smith.
