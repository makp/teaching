
# Week 5: Constraints on Perfection

-   02-27

    -   Peer-review workshop

-   03-01

    -   F. Jacob "[Evolution and tinkering](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" *Science*.


# Week 6: Replaying Life's Tape

-   03-06

    -   J. Coyne "[Remnants: vestiges, embryos, and bad design](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Why
        Evolution is True*.

-   03-08

    -   S. Gould "[The Iconography of an Expectation](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Wonderful Life*.


# Week 7: Complexity and Evolutionary Progress

-   03-13

    -   S. Gould "[Darwinian Fundamentalism](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *The New York Review of
        Books*.
    -   R. Dawkins "[Forty-five thousand generations of evolution in the lab](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)"
        from *The Greatest Show on Earth*.

-   03-15

    -   R. Dawkins "[Human Chauvinism](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" *Evolution*


# Week 8: *Spring Break*

-   03-20

    -   <span class="underline">No class</span> (Spring Break)

-   03-22

    -   <span class="underline">No class</span> (Spring Break)

