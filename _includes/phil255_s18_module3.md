
# Week 6

-   03-06

    -   M. Ridley "[Ecology as Religion](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" from *The Origins of Virtue*.

-   03-08

    -   J. Diamond ["Easter's End"](http://discovermagazine.com/1995/aug/eastersend543) *Discover Magazine*.


# Week 7

-   03-13

    -   G. Hardin "[The Tragedy of the Commons](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Science*

-   03-15

    -   G. Hardin "[Extensions of the Tragedy of the Commons](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Science*


# Week 8

-   03-20

    -   <span class="underline">Spring break</span>

-   03-22

    -   <span class="underline">Spring break</span>


# Week 9

-   03-27

    -   H. Baetjer Jr "[Free Market Incentives Foster Service to Others](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" from
        *Free our Markets*

-   03-29

    -   No required readings.


# Week 10

-   04-03

    -   M. Valo "[Guadeloupe and Martinique threatened as pesticide
        contaminates food chain](https://www.theguardian.com/environment/2013/may/07/guadeloupe-economy-theatened-pesticides-pollution)" *The Guardian*

-   04-05

    -   D. Arnold and K. Bustos "[Business, Ethics, and Global Climate Change](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Business and Professional Ethics Journal*.


# Week 11

-   04-10

    -   Special guest: [Brian Richter](http://www.sustainablewaters.org/brian-richter/) (president of *[Sustainable Waters](http://www.sustainablewaters.org/)*).

-   04-12

    -   <span class="underline">Exam 2</span>

