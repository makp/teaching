
# Week 11: The Problem of the Commons

-   04-10

    -   Peer-review workshop

-   04-12

    -   J. Greene's "[The Tragedy of the Commons](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Moral Tribes*.
    -   Special guest: *Fred Smalkin*.
        -   Optional readings (suggested by Fred Smalkin):
            -   P. Leeson "[Why the trial by ordeal was actually an effective test
                of guilt](https://aeon.co/ideas/why-the-trial-by-ordeal-was-actually-an-effective-test-of-guilt)" *Aeon*.
            -   H. Demsetz "[Toward a Theory of Property Rights](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" *The American
                Economic Review*.
            -   J. Goodman "[An Economic Theory of the Evolution of Common Law](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)"
                *The Journal of Legal Studies*.


# Week 12: The Social Contract

-   04-17

    -   Ridley "[The Power of Property](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *The Origins of Virtue*.

-   04-19

    -   J. Rachels "[The Social Contract Theory](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" *The Elements of Moral Philosophy*.
    -   Y. Bhattacharjee ["The Science Behind Psychopaths and Extreme Altruists"](https://www.nationalgeographic.com/magazine/2017/08/science-good-evil-charlottesville/) *The National Geographic Magazine*.


# Week 13: Social Instincts

-   04-24

    -   F. de Waal "[Morally Evolved](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Primates and Philosophers*.

-   04-26

    1.  J. Greene "[Efficiency, Flexibility, and the Dual-Process Brain](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from
        *Moral Tribes*.
    2.  J. Greene "[Cushman et al's experiment on human aggression](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from
        *Moral Tribes*.
    3.  A. Damasion "[Unpleasantness in Vermont](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Descartes' Error*.


# Week 14: Free-riding and self-deception

-   05-01

    -   J. Darley and B. Latane "[Bystander Intervention In Emergencies](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)"
        *Journal of Personality and Social Psychology*.
    -   L. Dugatkin "[What's in It for Me?](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Cheating Monkeys and
        Citizen Bees*.

-   05-03

    -   R. Trivers' "[The Evolutionary Logic of Self-Deception](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *The
        Folly of Fools*.


# Week 15: The Evolution of Altruism

-   05-08

    -   N. Angier "[Theorists See Evolutionary Advantages In Menopause](https://www.nytimes.com/1997/09/16/science/theorists-see-evolutionary-advantages-in-menopause.html)" *The
        New York Times*
    -   L. Dugatkin "[For the Good of Others?](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *Cheating Monkeys and
        Citizen Bees*.

-   05-10

    -   Ridley "[The Prisoner's Dilemma](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)" from *The Origins of Virtue*.


# Week 16: Course conclusion

-   05-15

    -   Peer-review


# Final Exam

-   May 17 at 12:30 PM

