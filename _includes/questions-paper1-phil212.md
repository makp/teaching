

## Questions -- First paper

I. In his book *Darwin's Dangerous Idea*, the philosopher Daniel
    Dennett makes the following remark regarding Darwin's theory of
    evolution by natural selection:

> Let me lay my cards on the table. If I were to give an award for the
> single best idea anyone has ever had, I'd give it to Darwin, ahead
> of Newton and Einstein and everyone else. In a single stroke, the
> idea of evolution by natural selection unifies the realm of life,
> meaning, and purpose with the realm of space and time, cause and
> effect, mechanism and physical law.

According to Dennett, Darwin introduced a novel way of explaining
complexity with his theory of evolution by natural selection. Use the
sources discussed in class to write a four page paper (around 1100
words) that supports Dennett's position [85pts].

II.  Consider the following argument:

> Natural selection cannot explain why you and I have eyes and ears or
> why spiders spin their webs. Natural is not a positive force that
> creates adaptations because natural selection has to be distinguished
> from mutation. Random mutations create genetic plans, and natural
> selection chooses between them&#x2014;not by creating the good ones, just by
> destroying bad ones.

Do you find this argument convincing? Read the excerpt from Peter
Godfrey-Smith's book *Philosophy of Biology* available at [this
link](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/Eh293fCevodBlWN_XIRJxboBKVSdNLplwCVt08MKC8qBAg?e=MPUDRx)
and write a two page (around 500 words) analysis of the argument above
[15pts]. (*Note*: your analysis must explain in your own words the
distinction between origin and distribution explanations, and the rest
of your analysis should build on this distinction).

