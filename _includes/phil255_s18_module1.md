
# Week 1

-   01-30

    -   No required readings.

-   02-01

    -   J. Rachels & S. Rachels ["The Baby Theresa Case"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) from *The Elements
        of Moral Philosophy*.


# Week 2

-   02-06

    -   No required readings

-   02-08

    -   J. Rachels & S. Rachels ["The Utilitarian Approach"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) from *The
        Elements of Moral Philosophy*

