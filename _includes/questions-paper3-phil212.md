## Prompt -- Third Paper

1.  As J. Greene puts it, the *problem of cooperation* is 'the problem
    of getting collective interest to triumph over individual interest,
    when possible.' Use Greene's "The Tragedy of the Commons" and Darley
    and Latane's "Bystander Intervention in Emergencies" to explain why
    people sometimes fail to cooperate with others.
2.  Most of the readings in this module focused on different solutions
    to the problem of cooperation. Use at least three of these readings
    to explain how collective interest can sometimes triumph over
    individual interest.