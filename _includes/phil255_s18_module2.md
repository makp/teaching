
# Week 3

-   02-13

    -   J. Sachs ["An Unequal World"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) from *The Age of Sustainable Development*.

-   02-15

    -   P. Singer ["Rich and Poor"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) from *Practical Ethics*.


# Week 4

-   02-20

    -   G. Hardin ["Living on a Lifeboat"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) *BioScience*.

-   02-22

    -   J. Sachs et al ["The Geography of Poverty and Wealth"](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e) *Scientific American*.


# Week 5

-   02-27

    -   No required reading (review).

-   03-01

    -   <span class="underline">Exam 1</span>

