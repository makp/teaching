
# Week 12

-   04-17

    -   J. Rachels "[Are humans the only moral animals?](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" from *Created from Animals*.

-   04-19

    -   P. Singer "[All animals are equal](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" from *Animal Liberation*.


# Week 13

-   04-24

    -   No required readings.

-   04-26

    -   M. Sagoff "[Animal Liberation and Environmental Ethics](https://tu-my.sharepoint.com/:f:/g/personal/mpedroso_towson_edu/EjS2tHEq8f1FpYhF866AXZ4BOJHnvuphHKLd9fyFEBECGg?e=hVmT8e)" *Osgoode Hall
        Law Journal*

